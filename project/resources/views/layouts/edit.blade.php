@extends('layouts.default')
@section('content')
    <div class="row">
        <section class="content">
            <div class="col-md-8 col-md-offset-2">
                @if(count($errors) > 0)
                    <div class="alert alert-danger">
                        <strong>Ops!</strong>Algo deu errado<br><br>
                    </div>
                    <ul>
                        @foreach($errors->all() as $error)
                            <li>{{$error}}</li>
                        @endforeach
                    </ul>
            </div>
                @endif
                @if(Session::has('success'))
                    <div class="alert alert-info">
                        {{Session::get('success')}}
                    </div>
                @endif
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">Update conta: {{$conta->nome}}</h3>
                </div>
                <div class="panel-body">
                    <div class="table-container">
                        <form method="post" action="{{route('conta.update', $conta->id)}}" role="form">
                            {{csrf_field()}}
                            <input type="hidden" name="">
                            <div class="row">
                                <div class="col-xs-6 col-sm-6">
                                    <div class="form-group">
                                        <input type="text" name="nome" id="nome_conta" value="{{$conta->nome}}" class="form-control input-sm" placeholder="Nome da conta">
                                    </div>
                                </div>
                                <div class="col-xs-6 col-sm-6">
                                    <div class="form-group">
                                        <input type="text" name="valor" id="valor_conta" value="{{$conta->valor}}" class="form-control input-sm">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <textarea name="descricao" id="descricao_conta" class="form-control input-sm" placeholder="Descrição conta">{{$conta->descricao}}</textarea>
                            </div>
                            <div class="row">
                                <div class="col-xs-12 col-sm-12 col-md-12">
                                    <input type="submit" value="Update" class="btn btn-success btn-block">
                                    <a href="{{route('conta.index')}}" class="btn btn-info btn-block">Back</a>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection
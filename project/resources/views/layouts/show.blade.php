@extends('layouts.default')
@section('content')
    <div class="row">
        <section class="content">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">{{$conta->nome}}</h3>
                    </div>
                    <div class="panel-body">
                        <div class="table-container">
                            <div class="conta" style="text-align:center">
                                {{$conta->valor}}">
                            </div>
                            <div class="conta">
                                {{$conta->descricao}}
                            </div>
                            <div class="row">
                                <div class="col-xs-12 col-sm-12 col-md-12">
                                    <a href="{{route('conta.index')}}" class="btn btn-info btn-block">Voltar</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection
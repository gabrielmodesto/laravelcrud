@extends('layouts.default')
@section('content')
    <div class="row">
        <section class="content">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel-body">
                    <div class="pull-left">
                        <h3>Lista de contas</h3>
                    </div>
                    <div class="pull-right">
                        <div class="btn-group">
                            <a href="{{route('conta.create')}}" class="btn btn-info">Add</a>
                        </div>
                    </div>
                    <div class="table-container">
                        <table id="mytable" class="table table-bordred table-striped">
                            <thead>
                                <th><input type="checkbox" name="" id="checkall"></th>
                                <th>Nome</th>
                                <th>Valor</th>
                                <th>Data</th>
                                <th>Descricao</th>
                                <th>View</th>
                                <th>Edit</th>
                                <th>Delete</th>
                            </thead>
                            <tbody>
                                @if($conta->count())
                                    @foreach($conta as $contas)
                                        <tr>
                                            <td><input type="checkbox" name="" id="checkthis"></td>
                                            <td>{{$contas->nome}}</td>
                                            <td>{{$contas->valor}}</td>
                                            <td>{{$contas->data}}</td>
                                            <td>{{$contas->descricao}}</td>
                                            <td>
                                                <a href="{{action('ContaController@show', $contas->id)}}" class="btn btn-primary btn-xs"><span class="glyphicon glyphicon-eye-open"></span></a></td>
                                            <td>
                                                <a href="{{action('ContaController@edit', $contas->id)}}" class="btn btn-primary btn-xs"><span class="glyphicon glyphicon-pencil"></a>
                                            </td>
                                            <td>
                                                <form action="{{action('ContaController@destroy', $conta->id)}}" method="post">
                                                    {{csrf_field()}}
                                                    <input type="hidden" name="_method" value="DELETE">
                                                    <button class="btn btn-danger btn-xs" type="submit"><span class="glyphicon glyphicon-trash"></span></button></form>

                                            </td>
                                        </tr>
                                    @endforeach
                                    @else
                                        <tr>
                                            <td colspan="7">Sem contas encontradas</td>
                                        </tr>
                                    @endif
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </section>
        @endsection
    </div>
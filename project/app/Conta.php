<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Conta extends Model
{
    //
    protected $fillable = ['nome','valor','data','descricao'];
}
